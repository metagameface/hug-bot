const fs = require("fs");

function makeRemindmeModule(client, homedir) {
	const reminderFile = homedir + "/reminders.json";

	var reminders = [];
	var timeouts = [];

	//all intervals a reminder is able to recur by
	const recurIntervals = [
		// 1000 * 30 (30 seconds, use only when testing)
		1000 * 60 * 5,           // 5 mins
		1000 * 60 * 10,          // 10 mins
		1000 * 60 * 15,          // 15 mins
		1000 * 60 * 20,          // 20 mins
		1000 * 60 * 30,          // 30 mins
		1000 * 60 * 60,          // 1 hour
		1000 * 60 * 60 * 2,      // 2 hours
		1000 * 60 * 60 * 3,      // 3 hours
		1000 * 60 * 60 * 4,      // 4 hours
		1000 * 60 * 60 * 6,      // 6 hours
		1000 * 60 * 60 * 12,     // 12 hours
		1000 * 60 * 60 * 24,     // 1 day
		1000 * 60 * 60 * 24 * 2, // 2 days
		1000 * 60 * 60 * 24 * 3, // 3 days
		1000 * 60 * 60 * 24 * 4, // 4 days
		1000 * 60 * 60 * 24 * 5, // 5 days
		1000 * 60 * 60 * 24 * 6, // 6 days
		1000 * 60 * 60 * 24 * 7, // 1 week
	];

	//TODO: This is likely to be useful in other modules, put this in a utils module if it would be
	function withMessage(messageID, channelID, callback) {
		let cached = client.Messages.get(messageID);
		if(cached) {
			callback(cached);
		}	else {
			client.Channels.get(channelID).fetchMessages(1, messageID + 1, messageID - 1)
				.then(fetched => {
					callback(fetched.messages[0]);
				}).catch(e => {
					console.log('fetch failed');
					console.log(e);
				});
		}
	}

	//Allows for timeouts of intervals longer than 32 bits
	//TODO: This is likely to be useful in other modules, put this in a utils module if it would be
	function setLongTimeout(callback, millis, timeoutHandleObject = {}) {
		if(millis > 2147483647) {
			timeoutHandleObject.timeout = setTimeout((() => setLongTimeout(callback, millis - 2147483647, timeoutHandleObject)), 2147483647);
			return timeoutHandleObject;
		} else {
			timeoutHandleObject.timeout = setTimeout(callback, millis);
			return timeoutHandleObject;
		}
	}

	function loadReminders() {
		let data = fs.readFileSync(reminderFile, "utf8");
		try {
			reminders = JSON.parse(data);
			if(reminders) {
				reminders.forEach(prepareReminder);
			}
		} 
		catch (e) {
			reminders = [];
			saveReminders();
		}
	}

	function saveReminders() {
		if(reminders) {
			fs.writeFileSync(reminderFile, JSON.stringify(reminders));
		}
	}

	function saveAndPrepareNewReminder(reminder) {
		reminders.push(reminder);
		saveReminders();	
		prepareReminder(reminder);
	}

	function reminderText(reminder) {
		let messageText = `${reminder.nickMention}, this is your reminder ${reminder.reminderText}`;

		//If message was sent late, mention this
		if(reminder.millisLate >= 10000) {
			messageText += `\n(This reminder is ${millisToString(reminder.millisLate)} late because I was not connected)`;
		}

		return messageText;
	}

	function controlsMessageText(reminder) {
		let millisRemaining = reminder.time - Date.now();
		let messageText = "";

		if(reminder.timesReminded == 0) {
			if(reminder.isCancelled) 
				messageText = "(reminder cancelled)";
			else {
				messageText = `${reminder.nickMention}, I will remind you ${reminder.reminderText}, in ${millisToString(millisRemaining)}.`;

				if(reminder.isRecurring) 
					messageText += `\nAfterwards, this reminder will repeat every ${millisToString(reminder.recurMillis)}`;
			}
		} else {
			// For recurring reminders, the last reminder serves as the controls message, and will always start withthe reminder text
			messageText = reminderText(reminder);

			if(reminder.isCancelled) messageText += "\n(recurring reminder cancelled)";
			else {
				messageText += `\n I will remind you this once more in ${millisToString(millisRemaining)}.`;
				if(reminder.isRecurring)
					messageText += `\nAfterwards, this reminder will repeat every ${millisToString(reminder.recurMillis)}`;
			}
		}
		return messageText;
	}

	function updateControlsMessage(reminder) {
		withMessage(reminder.controlsMessageID, reminder.channelID, m => {
			m.edit(controlsMessageText(reminder)).catch(e => {
				console.log("Failed to edit controls message");
				console.log(e);
			});
		});
	}

	function deleteControlsMessage(reminder) {
		withMessage(reminder.controlsMessageID, reminder.channelID, m => {
			m.delete().catch(e => {
				console.log("Failed to delete controls message");
				console.log(e);
			});
		});
	}

	function makeControls(reminder) {
		withMessage(reminder.controlsMessageID, reminder.channelID, m => {
			m.addReaction('🚫'); // CANCEL THIS REMINDER
			m.addReaction('🔁'); // MAKE RECURRING
		});
	}

	function removeControls(reminder) {
		withMessage(reminder.controlsMessageID, reminder.channelID, m => {
			m.removeReaction('🚫');
			m.removeReaction('🔁');
		});
		removeRecurControls(reminder);
	}

	function makeRecurControls(reminder) {
		withMessage(reminder.controlsMessageID, reminder.channelID, m => {
			m.addReaction('⏮'); // MINIMUM (5 minutes)
			m.addReaction('◀'); // LOWER TIME
			m.addReaction('▶'); // INCREASE TIME
			m.addReaction('⏭'); // MAXIMUM (7 days)
		});
	}

	function removeRecurControls(reminder) {
		withMessage(reminder.controlsMessageID, reminder.channelID, m => {
			m.removeReaction('⏮');
			m.removeReaction('◀');
			m.removeReaction('▶');
			m.removeReaction('⏭');
		});
	}

	function handleReminder(e) {
		let terseRegex = /!remind ?me (?:in )?((?:\d+[wdhms] ?)+) (.+)/;
		let verboseRegex = /!remind ?me (?:in )?(\d+ ?(?:second|seconds|minute|minutes|hour|hours|day|days|week|weeks)(?:(?:,| and|, and|) \d+ ?(?:second|seconds|minute|minutes|hour|hours|day|days|week|weeks))*) (.+)/;
		let terseMatch = e.message.content.match(terseRegex);
		let verboseMatch = e.message.content.match(verboseRegex);
		let parsed = terseMatch ? parseTime(terseMatch) : verboseMatch ? parseTime(verboseMatch) : null;
		if(parsed) {
			let [reminderMillis, reminderText] = parsed;
			let reminder = {
				time: Date.now() + reminderMillis, //Time at which reminder is to be given
				userID: e.message.author.id, //ID for the user requesting the reminder (TODO: Upgrade script changing user to this)
				nickMention: e.message.author.nickMention, //nickMention string for the user requesting the reminder
				reminderText: reminderText, //User-specified portion of the reminder message
				channelID: e.message.channel.id, //ID for the channel reminder is in (TODO: Upgrade script changing channel to this)
				retryMillis: 0, //number of extra millis to wait before retrying if send failed, starts at 1000 and doubles
				millisLate: 0, //number of milliseconds late the (last occurence of the) reminder was sent, if late enough this gets mentioned
				controlsMessageID: null, // ID of the message that the "controls" (done through emote reactions) for this reminder are on
				isCancelled: false, // whether this reminder has been cancelled
				isRecurring: false, // whether this reminder is recurring
				recurMillis: 0, // if recurring, how many milliseconds between recurrences
				timesReminded: 0 // number of times this reminder has been given
			}
			e.message.channel.sendMessage(controlsMessageText(reminder))
				.then((message) => {
					reminder.controlsMessageID = message.id;
					saveAndPrepareNewReminder(reminder);
					makeControls(reminder);
				})
		} else if(e.message.content.startsWith("!remind")) {
			e.message.addReaction("❌");
			e.message.channel.sendMessage("Proper syntax looks like '!remindme in [amount of time] [thing you need me to remind you], for instance, '!remindme in 1 hour to hug a catgirl', '!remind me in 5 minutes and 30 seconds to drain pasta' or '!remindme in 1d5h30m to fetch my slave from the airport");
		}

		function parseTime(matchResult) {
			const millis = {
				second: 1000,
				seconds: 1000,
				s: 1000,
				minute: 1000 * 60,
				minutes: 1000 * 60,
				m: 1000 * 60,
				hour: 1000 * 60 * 60,	
				hours: 1000 * 60 * 60,
				h: 1000 * 60 * 60,
				day: 1000 * 60 * 60 * 24,
				days: 1000 * 60 * 60 * 24,
				d: 1000 * 60 * 60 * 24,
				week: 1000 * 60 * 60 * 24 * 7,
				weeks: 1000 * 60 * 60 * 24 * 7,
				w: 1000 * 60 * 60 * 24 * 7
			};
			let timeComponent = /(\d+) ?(second|seconds|s|minute|minutes|m|hour|hours|h|day|days|d|week|weeks|w)/g;
			let reminderMillis = 0;
			let reminderText = matchResult[2];
			let nextMatch = null;
			while((nextMatch = timeComponent.exec(matchResult[1])) !== null) {
				reminderMillis += parseInt(nextMatch[1]) * millis[nextMatch[2]];
			}

			return [reminderMillis, reminderText];
		}
	}

	//Sets a timer to send a reminder message
	function prepareReminder(reminder) {
		let millisLeft = reminder.time - Date.now();
		//Send it now if expired (or if it's literally due this millisecond somehow)
		if(millisLeft <= 0 && reminder.retryMillis === 0) {
			remind(reminder);
		}
		else {
			timeouts.push(setLongTimeout(() => remind(reminder), millisLeft + reminder.retryMillis));
		}
	}

	//Cancel a reminder, but leave its setTimeout going to make it simpler to let it be uncancelled
	function cancelReminder(reminder) {
		reminder.isCancelled = true;
		saveReminders();
		updateControlsMessage(reminder);
	}

	function uncancelReminder(reminder) {
		reminder.isCancelled = false;
		saveReminders();
		updateControlsMessage(reminder);
	}

	function toggleRecurring(reminder) {
		if(reminder.isRecurring) unrecurReminder(reminder);
		else recurReminder(reminder);
	}

	function recurReminder(reminder) {
		reminder.isRecurring = true;
		// Default to 1 day, or the previous interval if had previously been recurring
		if(reminder.recurMillis == 0) reminder.recurMillis = 1000 * 60 * 60 * 24;
		saveReminders();
		updateControlsMessage(reminder);
		makeRecurControls(reminder);
	}

	function unrecurReminder(reminder) {
		reminder.isRecurring = false;
		saveReminders();
		updateControlsMessage(reminder);
		removeRecurControls(reminder);
	}

	function setRecurIntervalToMinimum(reminder) {
		reminder.recurMillis = recurIntervals[0];
		saveReminders();
		updateControlsMessage(reminder);
	}

	function setRecurIntervalToMaximum(reminder) {
		reminder.recurMillis = recurIntervals[recurIntervals.length - 1];
		saveReminders();
		updateControlsMessage(reminder);
	}

	function decreaseRecurInterval(reminder) {
		let currentIntervalIndex = recurIntervals.indexOf(reminder.recurMillis);
		if(currentIntervalIndex > 0) {
			reminder.recurMillis = recurIntervals[currentIntervalIndex - 1];
			saveReminders();
			updateControlsMessage(reminder);
		}
	}

	function increaseRecurInterval(reminder) {
		let currentIntervalIndex = recurIntervals.indexOf(reminder.recurMillis);
		if(currentIntervalIndex < recurIntervals.length - 1) {
			reminder.recurMillis = recurIntervals[currentIntervalIndex + 1];
			saveReminders();
			updateControlsMessage(reminder);
		}
	}

	function remind(reminder) {
		if(!reminder.isCancelled) {
			let channel = client.Channels.get(reminder.channelID);
			reminder.millisLate = Date.now() - reminder.time;
			let messageToSend = reminderText(reminder);

			channel.sendMessage(messageToSend)
				.then((message) => {
					reminder.timesReminded++;
					deleteControlsMessage(reminder);

					if(!reminder.isRecurring) {
						//reminder sent successfully, remove it from file
						reminders = reminders.filter((r) => r !== reminder);
						saveReminders();
					} else {
						//Reminder is recurring, requeue it and set the new time to the original time plus the recur interval
						//In case reminder was late, add lowest multiple of recur interval to the time that makes the next time it's due not late
						while(reminder.time < Date.now()) { //too lazy to math but this should do
							reminder.time += reminder.recurMillis;
						}

						//Make the reminder just sent serve as the new controls message
						reminder.controlsMessageID = message.id;
						updateControlsMessage(reminder);

						saveReminders();
						prepareReminder(reminder);
						makeControls(reminder);
						makeRecurControls(reminder);
					}

				}).catch((e) => {
					console.log(`Failed to send message: ${messageToSend} after retry time ${reminder.retryMillis}`);
					console.log(e);

					if(reminder.retryMillis === 0)
						reminder.retryMillis = 1000;
					else
						reminder.retryMillis *= 2;

					prepareReminder(reminder);
				});
		} else {
			//reminder was cancelled and is no longer due, remove it from file
			reminders = reminders.filter((r) => r !== reminder);
			saveReminders();
			updateControlsMessage(reminder);
			removeControls(reminder);
		}
	}

	function handleReact(reactEvent) {
		//CANCEL REMINDER
		if(reactEvent.emoji.name === '🚫') {
			let reminder = reminders.find(r => r.userID === reactEvent.data.user_id && r.controlsMessageID === reactEvent.data.message_id)
			if(reminder) {
				cancelReminder(reminder);
			}
		}
		//RECUR REMINDER
		if(reactEvent.emoji.name === '🔁') {
			let reminder = reminders.find(r => r.userID === reactEvent.data.user_id && r.controlsMessageID === reactEvent.data.message_id)
			if(reminder) {
				toggleRecurring(reminder);
			}
		}
		//SET RECUR INTERVAL TO MINIMUM
		if(reactEvent.emoji.name === '⏮') {
			let reminder = reminders.find(r => r.userID === reactEvent.data.user_id && r.controlsMessageID === reactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				setRecurIntervalToMinimum(reminder);
			}
		}
		//DECREASE RECUR INTERVAL
		if(reactEvent.emoji.name === '◀') {
			let reminder = reminders.find(r => r.userID === reactEvent.data.user_id && r.controlsMessageID === reactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				decreaseRecurInterval(reminder);
			}
		}
		//INCREASE RECUR INTERVAL
		if(reactEvent.emoji.name === '▶') {
			let reminder = reminders.find(r => r.userID === reactEvent.data.user_id && r.controlsMessageID === reactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				increaseRecurInterval(reminder);
			}
		}
		//SET RECUR TIME TO MAXIMUM
		if(reactEvent.emoji.name === '⏭') {
			let reminder = reminders.find(r => r.userID === reactEvent.data.user_id && r.controlsMessageID === reactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				setRecurIntervalToMaximum(reminder);
			}
		}
	}

	function handleUnreact(unreactEvent) {
		//UNCANCEL REMINDER
		if(unreactEvent.emoji.name === '🚫') {
			let reminder = reminders.find(r => r.userID === unreactEvent.data.user_id && r.controlsMessageID === unreactEvent.data.message_id)
			if(reminder) {
				uncancelReminder(reminder);
			}
		}
		//RECUR REMINDER
		if(unreactEvent.emoji.name === '🔁') {
			let reminder = reminders.find(r => r.userID === unreactEvent.data.user_id && r.controlsMessageID === unreactEvent.data.message_id)
			if(reminder) {
				toggleRecurring(reminder);
			}
		}
		//SET RECUR INTERVAL TO MINIMUM
		if(unreactEvent.emoji.name === '⏮') {
			let reminder = reminders.find(r => r.userID === unreactEvent.data.user_id && r.controlsMessageID === unreactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				setRecurIntervalToMinimum(reminder);
			}
		}
		//DECREASE RECUR INTERVAL
		if(unreactEvent.emoji.name === '◀') {
			let reminder = reminders.find(r => r.userID === unreactEvent.data.user_id && r.controlsMessageID === unreactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				decreaseRecurInterval(reminder);
			}
		}
		//INCREASE RECUR INTERVAL
		if(unreactEvent.emoji.name === '▶') {
			let reminder = reminders.find(r => r.userID === unreactEvent.data.user_id && r.controlsMessageID === unreactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				increaseRecurInterval(reminder);
			}
		}
		//SET RECUR TIME TO MAXIMUM
		if(unreactEvent.emoji.name === '⏭') {
			let reminder = reminders.find(r => r.userID === unreactEvent.data.user_id && r.controlsMessageID === unreactEvent.data.message_id && r.isRecurring)
			if(reminder) {
				setRecurIntervalToMaximum(reminder);
			}
		}
	}

	function millisToString(millis) {
		if(millis === 1) return "1 millisecond"
		else if(millis < 1000) return `${millis} milliseconds`;
		else {
			let totalSeconds = Math.round(millis / 1000);
			let totalMinutes = totalSeconds < 60 ? 0 : Math.floor(totalSeconds / 60);
			let totalHours = totalMinutes < 60 ? 0 : Math.floor(totalMinutes / 60);
			let totalDays = totalHours < 24 ? 0 : Math.floor(totalHours / 24);
			let weeks = totalDays < 7 ? 0 : Math.floor(totalDays / 7);
			let days = totalDays - (weeks * 7);
			let hours = totalHours - (totalDays * 24);
			let minutes = totalMinutes - (totalHours * 60);
			let seconds = totalSeconds - (totalMinutes * 60);

			let weeksPart = weeks === 1 ? "1 week" : `${weeks} weeks`;
			let daysPart = days === 1 ? "1 day" : `${days} days`;
			let hoursPart = hours === 1 ? "1 hour" : `${hours} hours`;
			let minutesPart = minutes === 1 ? "1 minute" : `${minutes} minutes`;
			let secondsPart = seconds === 1 ? "1 second" : `${seconds} seconds`;

			let parts = [[weeks, weeksPart], [days, daysPart], [hours, hoursPart], [minutes, minutesPart], [seconds, secondsPart]]
				.filter(pair => pair[0] > 0)
				.map(pair => pair[1]);

			if(parts.length > 1) parts[parts.length - 1] = "and " + parts[parts.length - 1];
			if(parts.length > 2)
				return parts.join(', ');
			else
				return parts.join(' ');
		}
	}
	
	const remindmeModule = {
		moduleName: "remindme",
		setup: function() {			
			//create reminder file if not existing
			try {
				fs.writeFileSync(reminderFile, JSON.stringify([]), {flag: "wx"});
			} catch (e) {} //Error if file already exists, this is fine
		},
		onConnect: loadReminders,
		onReconnect: loadReminders,
		onDisconnect: function() {
			console.log("Disconnected");
			timeouts.forEach(t => clearTimeout(t.timeout));
			timeouts = [];
		},
		onMessageReact: handleReact,
		onMessageUnreact: handleUnreact,
		messageHandlers: [handleReminder]
	}

	return remindmeModule;
}

module.exports = makeRemindmeModule