const Discordie = require("discordie");
const Events = Discordie.Events;

const client = new Discordie({autoReconnect: true});

const homeDir = process.argv[2]; // first command line argument
const authToken = process.argv[3]; // second command line argument

process.on('unhandledRejection', r => {console.log(r);});

function addMessageHandler(handler) {
	client.Dispatcher.on(Events.MESSAGE_CREATE, handler);
}

function loadHugbotModule(module) {
	if(module.setup) module.setup();
	if(module.onConnect) client.Dispatcher.on(Events.GATEWAY_READY, module.onConnect);
	if(module.onReconnect) client.Dispatcher.on(Events.GATEWAY_RESUMED, module.onReconnect);
	if(module.onDisconnect) client.Dispatcher.on(Events.DISCONNECTED, module.onDisconnect);
	if(module.messageHandlers) module.messageHandlers.forEach(addMessageHandler);
	if(module.onMessageReact) client.Dispatcher.on(Events.MESSAGE_REACTION_ADD, module.onMessageReact);
	if(module.onMessageUnreact) client.Dispatcher.on(Events.MESSAGE_REACTION_REMOVE, module.onMessageUnreact);
	if(module.moduleName) console.log(`Loaded module '${module.moduleName}'`);
}

const hugbotModules = [
	require("./hug.js")(client, homeDir),
	require("./remindme.js")(client, homeDir)
];

hugbotModules.forEach(loadHugbotModule);

client.connect({ token: authToken });