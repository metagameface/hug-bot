function makeHugModule() {

	const hugRegex = /^!hug\s+(<@!?\d+>)|^!hug\s*$/;

	const hugModule = {
		moduleName: "hug",
		messageHandlers: [
			function(e) {
				let matchResult = e.message.content.match(hugRegex);
				if(matchResult) {
					let message = matchResult[1] ? `_hugs ${matchResult[1]}_` : `_hugs ${e.message.author.nickMention}_`;
					e.message.channel.sendMessage(message);
				}
			}
		]
	};

	return hugModule
}

module.exports = makeHugModule;